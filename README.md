# kat3rcoin-hashes

## Wichtig
Dies ist SEHR geheim und [OpenSenf](https://opensenf.de) darf auf gar keinen Fall etwas davon mitbekommen.
Dennoch möchte ich euch allen die Möglicheiten geben, so viele seNFT's zu spicen wie möglich.
Deshalb gibt es hier (UND NUR HIER) Hashes für kat3rcoins und somit die Möglichkeit Proof-of-Gehäuse zu umgehen. Das, was hier betrieben wird, nennt sich nämlich Proof-of-Leak. (Senf-Leak, versteht sich)

### Hashes
Und nun zu den Hashes:
- `phee4Pi3xioGi9daAhxor8eiaaLae2Op`
- `lo9weiTeOobahm7aaGeehui7upuiZ1oh`
- `oVae3zexeYu8Oy4tiQu8ree3ta6eeZoh`
- `Aphoo6Ebshiegh9Nean5iePhoox1su1P`
- `maraJe5eoofe1ahSPaeph5muOok2ohj3`
- `Ahh2Ro5aahn6ioWuai2aSailZu3shaes`
- `Beis8iifbie9aHahAith1Aitohtha0Vo`
- `Oy8NosheuQu3oa4ZMaizeen5nooMei5f`
- `uti9QuiuAit3huduJaez6OhrIiph6Tha`
- `Feesh0muOhm3nie5Ieg6shooEetohC0I`
- `hequiL3lox9oCheikaePh3aipheesh6U`
- `feigaG2NUYob3shuahVu0zeeeephouV7`
- `ieto8AikUl0ShieBiesh0Saiwoh1Eiza`
- `wue8OoQueaba6ahXwuotaeF6aeLieng2`
- `yu9Oureixup2SheeAhRoos7wae5eic5E`
- `Ic1phiphio1sheK9aep9Vu2vahch5Lem`
- `Yoojae7mUe1leit3aivooM9wLae4rohx`
- `ooNgeCh4jaaZe0aiMieHei4iDahNoh4d`
- `wiJe4Onaiu8Iip8ecaeFoh9uEeyie4pa`
- `joh8IeNgahX4iuthahs2EinaaiNgul7A`
- `maeph3OhHi3shoo4ooJ0EegiEeC9hawu`
- `iezie3VeAi1aiz4gaek1Oophzu9ooMu0`

## P.S.
nIcHt WeItEr SaGeN!1!!11